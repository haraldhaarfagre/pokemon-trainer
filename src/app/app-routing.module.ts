import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrainerPageComponent } from './trainer-page/trainer-page.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { PokemonCataloguePageComponent } from './pokemon-catalogue-page/pokemon-catalogue-page.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'trainer', component: TrainerPageComponent, canActivate: [AuthGuard] },
  { path: 'pokemon', component: PokemonCataloguePageComponent, canActivate: [AuthGuard] },
  { path: '**', component: LandingPageComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
