import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pokemon } from '../pokemon-catalogue-page/pokemon';


@Injectable({
    providedIn: 'root'
})
export class PokemonService {
    
    private pokemonUrl = 'https://pokeapi.co/api/v2/pokemon?limit=1118';
    
    // httpOptions = {
    //     headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    // };
    constructor(private http: HttpClient) { }
    
    getPokemons(): Observable<String[]>{
        return this.http.get<String[]>(this.pokemonUrl)
         .pipe(
             map( res => Object.values(res))
          )
    }
}