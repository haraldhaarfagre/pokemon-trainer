import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
  export class UserService {
    public username = '';
    login(username: string) {
      this.username = username;
      localStorage.setItem('username', username);
    }
    isLoggedIn(): boolean {
      const savedUsername = localStorage.getItem('username');
      return Boolean(savedUsername)
    }
    logout(): void {
      this.username = '';
      localStorage.removeItem('username');
    }
  }