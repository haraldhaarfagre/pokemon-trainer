import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from './userService';

@Component({
  selector: 'app-landing-page',
  //templateUrl: './landing-page.component.html',
  template: `
  <main class="mt-3 mb-3 mp-3 ml-3 mr-3">
    <form (ngSubmit)="onSubmit()">
    <div class="form-group mt-3 mb-3 ml-3 mr-3">
      <h1>welcome to the Pokémon trainer app</h1>
      <h3>you need to login to continue</h3>
  
      
       Username: 
       <input type="text" class="form-control" id="name" required [(ngModel)]="username" name="test">
      
      <button type="submit" class="btn btn-success" >Submit</button>
    </div>
    </form>
  </main>
  `,
  //Username: <input type="text" [formControl]="userNameControl">

  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor(private readonly userService: UserService, private router: Router) { }
  username = '';
  
  ngOnInit(): void {
            }

  onSubmit() {
    
    this.userService.login(this.username);
    this.router.navigate(['/pokemon']);
  }
}
