import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { config } from 'rxjs';
import { PokemonService } from '../config/config.service';
import { Pokemon } from './pokemon';
import { __values } from 'tslib';

@Component({
  selector: 'app-pokemon-catalogue-page',
  template: `<main>
                <h3>Pokemons</h3>
              <ul>
              <li>
                {{pokemons[3] | json}}
              </li>
              <li *ngFor="let item of pokemons; let i = index;"> 
                  {{item}}
                  {{item}}
              </li>
            </ul>
            <p>{{pokemons[3] | json}}</p>
            </main>
            `,
  styleUrls: ['./pokemon-catalogue-page.component.css']
})
export class PokemonCataloguePageComponent implements OnInit {
  pokemons: String[] = [];
  constructor(private pokemonservice: PokemonService) { }
  //pokemons: JSON;
  //var pokelist = async getPokemons();
  // configUrl = 'https://pokeapi.co/api/v2/pokemon?limit=1118';
  ngOnInit(): void {
    this.getPokemons();
    console.log(this.pokemons[3]);
  }
  
  getPokemons(): void{
    this.pokemonservice.getPokemons()
          .subscribe((p) => (this.pokemons = p));
          // .subscribe((data: any) => {
          //   return data;
          // });
        // .map(data => __values(data));
  }
  


}
